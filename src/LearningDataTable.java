import java.sql.*;
import java.awt.*;
import java.util.*;
import javax.swing.*;


public class LearningDataTable{
public void learning() 
{
    Connection con = null;
    Statement st = null;
    ResultSet rs = null;
    String s;
    try{
   Class.forName("com.mysql.jdbc.Driver");
        con=DriverManager.getConnection("jdbc:mysql://localhost/health_tracking","root", "root");
        st= con.createStatement();
        s = "select * from health_tracking.learningdata";
        rs = st.executeQuery(s);
        ResultSetMetaData rsmt = rs.getMetaData();
        int c = rsmt.getColumnCount();
        Vector column = new Vector();
        for(int i = 1;i<=c;i++ )
        {
            column.add(rsmt.getColumnName(i));
        }
        Vector data = new Vector();
        Vector row = new Vector();
        while(rs.next())
        {
            row = new Vector();	
            for(int i =1;i<=c;i++)
            {
                row.add(rs.getString(i));
            }
            data.add(row);
            
        }
        JFrame frame = new JFrame();
        frame.setSize(1500,1200);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JTable table = new JTable(data,column);
        JScrollPane jsp = new JScrollPane(table);
        panel.setLayout(new BorderLayout());
        panel.add(jsp,BorderLayout.CENTER);
        frame.setContentPane(panel);
        frame.setVisible(true);
        
        
    }catch(SQLException e)
    {
        e.printStackTrace();
        JOptionPane.showMessageDialog(null,"ERROR");
        
    }catch(Exception e)
    {
        e.printStackTrace();
        JOptionPane.showMessageDialog(null,"ERROR CLOSE");
    }
}
}

import java.awt.Container;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.*;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.*;

public class CSVread extends JApplet {
  private String[] description = { "Select Data to be fetched","Learning Data", "Tracking Data"};

  private JComboBox c = new JComboBox();

  private JButton b = new JButton("Submit");
  
  
  abc obj = new abc();
  TrackingData td = new TrackingData();
  
  
  public void init()
  {
    for (int i = 0; i < 3; i++){
      c.addItem(description[i]);
    }
    c.setSelectedItem("Null");

    b.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    	  Object abc =  c.getSelectedItem();
    	  
    	  if(description[1].equals(abc)){    	
        	
    		 
    		  obj.getData();
        }
    	  else if (description[2].equals(abc)){
        	
    		  td.getData();
        	
        }
      }
    });
    
    
    
    Container cp = getContentPane();
    cp.setLayout(new FlowLayout());

    cp.add(c);
    cp.add(b);
    JTextArea textAreal = new JTextArea(30, 70);
    textAreal.setPreferredSize(new Dimension(300, 300));
    JScrollPane scrollPane = new JScrollPane(textAreal, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
   // cp.add(textAreal);
    cp.add(scrollPane);
  }

  public static void main(String[] args) {
    run(new CSVread(), 400, 225);
  }

  public static void run(JApplet applet, int width, int height) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(applet);
    frame.setSize(1400, 735);
    applet.init();
    applet.start();
    frame.setVisible(true);
  }
}